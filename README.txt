See project homepage: http://drupal.org/sandbox/raphaelhuefner/1748400

This module is similar to the simple content boxes provided by the Boxes
( http://drupal.org/project/boxes ) module itself, except that this type of box
displays different content depending on the language of the current page.

Development sponsored by Affinity Bridge http://affinitybridge.com
