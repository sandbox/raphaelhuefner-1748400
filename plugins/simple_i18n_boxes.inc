<?php

/**
 * @file
 * Provides a new multilingual box type.
 */

/**
 * Multilingual box.
 */
class simple_i18n_boxes extends boxes_box {
  protected function getLanguageCode() {
    global $language;
    return $language->language;
  }

  protected function getLanguageName() {
    global $language;
    return $language->name;
  }

  protected function getTitleFieldName($language_code = NULL) {
    return
      'title_'
      .
      (is_null($language_code) ? $this->getLanguageCode() : $language_code)
    ;
  }

  protected function getContentFieldName($language_code = NULL) {
    return
      'content_'
      .
      (is_null($language_code) ? $this->getLanguageCode() : $language_code)
    ;
  }

  /**
   * Implements boxes_box::options_defaults().
   */
  public function options_defaults() {
    $languages = language_list('language');
    $options = array();
    foreach ($languages as $langcode => $language) {
      $options[$this->getTitleFieldName($langcode)] = '';
      $options[$this->getContentFieldName($langcode)] = array(
        'value' => '',
        'format' => filter_default_format(),
      );
    }
    return $options;
  }

  /**
   * @see http://en.wikipedia.org/wiki/Template_method_pattern
   */
  protected function alterTitleValue($title_value) {
    return $title_value; // do nothing, allow for overriding in child class.
  }

  protected function getTitleValue() {
    $title_value =
      isset($this->options[$this->getTitleFieldName()])
      ?
      $this->options[$this->getTitleFieldName()]
      :
      ''
    ;
    return $this->alterTitleValue($title_value);
  }

  /**
   * @see http://en.wikipedia.org/wiki/Template_method_pattern
   */
  protected function alterContentValue($content_value) {
    return $content_value; // do nothing, allow for overriding in child class.
  }

  protected function getContentValue() {
    $content_value =
      isset($this->options[$this->getContentFieldName()]['value'])
      ?
      $this->options[$this->getContentFieldName()]['value']
      :
      ''
    ;
    return $this->alterContentValue($content_value);
  }

  /**
   * @see http://en.wikipedia.org/wiki/Template_method_pattern
   */
  protected function alterContentFormat($content_format) {
    return $content_format; // do nothing, allow for overriding in child class.
  }

  protected function getContentFormat() {
    $content_format =
      isset($this->options[$this->getContentFieldName()]['format'])
      ?
      $this->options[$this->getContentFieldName()]['format']
      :
      filter_default_format()
    ;
    return $this->alterContentFormat($content_format);
  }

  /**
   * Implements boxes_box::options_form().
   */
  public function options_form(&$form_state) {
    $t_var = array('@language' => $this->getLanguageName());
    $form = array();

    // Carry over all saved options.
    // @see http://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7#val
    foreach ($this->options as $fieldname => $fieldvalue) {
      $form[$fieldname] = array(
        '#type' => 'value',
        '#value' => $fieldvalue,
      );
    }

    // Expose as editable only those options relevant for the current language.
    $form[$this->getTitleFieldName()] = array(
      '#type' => 'textfield',
      '#title' => t('@language title', $t_var),
      '#description' => t('All languages other than @language are editable when seeing this page in those other languages.', $t_var),
      '#default_value' => $this->getTitleValue(),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => FALSE,
    );

    $form[$this->getContentFieldName()] = array(
      '#type' => 'text_format',
      '#base_type' => 'textarea',
      '#title' => t('@language content', $t_var),
      '#description' => t('All languages other than @language are editable when seeing this page in those other languages.', $t_var),
      '#format' => $this->getContentFormat(),
      '#default_value' => $this->getContentValue(),
    );

    return $form;
  }

  /**
   * Implements boxes_box::option_submit().
   */
  public function options_submit($form, &$form_state) {
    foreach ($form['options'] as $field) {
      if (isset($form_state['input'][$field])) {
        $this->options[$field] = $form_state['input'][$field];
      }
    }
  }

  /**
   * Implements boxes_box::render().
   */
  public function render() {
    /*
     * Don't use check_plain() for normal rendering of the title, i.e. no
     * $title = check_plain($title_value);
     *
     * Block module seems to do that for us somewhere else.
     * TODO Check whether this assumption is true.
     */
    $title = $this->getTitleValue();

    $content = check_markup(
      $this->getContentValue(),
      $this->getContentFormat(),
      $this->getLanguageCode(),
      FALSE
    );

    return array(
      'delta' => $this->delta,
      'title' => $title,
      'subject' => $title,
      'content' => $content,
    );
  }
}
